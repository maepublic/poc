export interface IFamilyTree {
        givenName: string; 
        familyName: string; 
        gender: 'male' | 'female';
   
}
