import express, { Request, Response } from 'express';
import { getAllData } from '../service/homeService';
const router = express.Router();

router.get('/', async (req: Request, res: Response) => {
    const data = await getAllData();
    res.render('home', { data });
});

export default router;
