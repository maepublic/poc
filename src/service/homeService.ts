import {Parser, Generator} from "sparqljs";
import { PREFIX } from "./sparql";
import { IFamilyTree } from "../models/IFamilyTree";

export const getAllData = async (): Promise<IFamilyTree[]> => {
    try {
        const fusekiUrl = "http://localhost:3030/family/query"; 
        const sparqlQuery = PREFIX + `
        SELECT ?givenName ?familyName ?gender
        WHERE {
            ?person foaf:gender ?gender;
                    foaf:givenName ?givenName;
                    foaf:familyName ?familyName.
        }
        `;
       
// Parse the SPARQL query
const parser = new Parser();
const parsedQuery = parser.parse(sparqlQuery);
// Convert query object back to SPARQL string
const serializedQuery = new Generator().stringify(parsedQuery);

    //Encode the SPARQL query
    const encodedQuery = encodeURIComponent(sparqlQuery);

    // Construct the URL with the query parameter
    const url = `${fusekiUrl}?query=${encodedQuery}&format=json`;

    // Make the GET request
     const response = await fetch(url);

      // const response = await fetch(fusekiUrl, {
      //   method: 'POST',
      //   headers: {
      //     'Content-Type': 'application/x-www-form-urlencoded',          
      //   },
      //   body: new URLSearchParams({
      //     query: serializedQuery,
      //     format: 'json', // Specify the response format
      //   }),
      // });
  
      if (!response.ok) {
        console.log(response)
        //throw new Error('Failed to execute SPARQL query');
      }

      const data = await response.json();
      // Process query results
      console.log('Query results:', data.results.bindings[0]);

      const results: IFamilyTree[] = data.results.bindings.map((binding: any) => ({
        givenName: binding.givenName.value,
        familyName: binding.familyName.value,
        gender: binding.gender.value
    }));
    console.log(results)
    return results;

        return data as unknown as IFamilyTree[];

    } catch (error) {
        console.error('Error fetching data from Fuseki:', error);
        // Handle the error, you can return an empty array or throw the error
        throw error;
    }
};
